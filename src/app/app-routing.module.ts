import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'fullrandomquotes',
    loadChildren: () =>
      import('./modules/full-randomquotes/full-randomquotes.module').then(
        (m) => m.FullRandomquotesModule
      ),
  },
  {
    path: 'randomquotes',
    loadChildren: () =>
      import('./modules/random-quotes/random-quotes.module').then(
        (m) => m.RandomQuotesModule
      ),
  },
  {
    path: 'staticpage1',
    loadChildren: () =>
      import('./modules/static-page-parent1/static-page-parent1.module').then(
        (m) => m.StaticPageParent1Module
      ),
  },
  {
    path: 'staticpage2',
    loadChildren: () =>
      import('./modules/static-page-parent2/static-page-parent2.module').then(
        (m) => m.StaticPageParent2Module
      ),
  },
  {
    path: 'authorlist',
    loadChildren: () =>
      import('./modules/author-list/author-list.module').then(
        (m) => m.AuthorListModule
      ),
  },
  {
    path: 'authorquote',
    loadChildren: () =>
      import('./modules/author-quote/author-quote.module').then(
        (m) => m.AuthorQuoteModule
      ),
  },
  { path: '**', redirectTo: 'staticpage1', pathMatch: 'full' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
