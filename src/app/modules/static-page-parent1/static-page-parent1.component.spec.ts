import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticPageParent1Component } from './static-page-parent1.component';

describe('StaticPageParent1Component', () => {
  let component: StaticPageParent1Component;
  let fixture: ComponentFixture<StaticPageParent1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StaticPageParent1Component],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticPageParent1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
