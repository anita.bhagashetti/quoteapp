import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StaticPageParent1RoutingModule } from './static-page-parent1-routing.module';
import { StaticPageParent1Component } from './static-page-parent1.component';

import { StaticPagePresentationModule } from 'src/app/components/static-page-presentation/static-page-presentation.module';
@NgModule({
  declarations: [StaticPageParent1Component],
  imports: [
    CommonModule,
    StaticPageParent1RoutingModule,
    StaticPagePresentationModule,
  ],
})
export class StaticPageParent1Module {}
