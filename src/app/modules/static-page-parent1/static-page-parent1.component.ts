import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-static-page-parent1',
  templateUrl: './static-page-parent1.component.html',
  styleUrls: ['./static-page-parent1.component.scss'],
})
export class StaticPageParent1Component implements OnInit {
  public staticData = {
    head: 'Train smarter, get stronger',
    button: 'start free trial',
    img: 'assets/istockphoto-1326064432-1024x1024-removebg-preview.png',
    about:
      'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Qui aut optio,aliquid repellendus harum voluptatum xplicabo modi quae maiores deleniti.,Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas dolor quos,commodi a nimi fugiat nobis d ucimus quibusdam sequi eius sed.',
  };

  constructor() {}

  ngOnInit(): void {}
}
