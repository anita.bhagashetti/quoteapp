import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StaticPageParent1Component } from './static-page-parent1.component';

const routes: Routes = [{ path: '', component: StaticPageParent1Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StaticPageParent1RoutingModule {}
