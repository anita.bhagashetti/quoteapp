import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthorListRoutingModule } from './author-list-routing.module';
import { AuthorListComponent } from './author-list.component';


@NgModule({
  declarations: [
    AuthorListComponent
  ],
  imports: [
    CommonModule,
    AuthorListRoutingModule
  ]
})
export class AuthorListModule { }
