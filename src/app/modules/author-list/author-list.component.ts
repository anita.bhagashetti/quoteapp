import { Component, OnInit } from '@angular/core';
import { DataServiceService } from 'src/app/services/data-service.service';

@Component({
  selector: 'app-author-list',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.scss'],
})
export class AuthorListComponent implements OnInit {
  imgUrlFlag: boolean = true;
  authors: any;
  defaultImage: string =
    'https://cdn3.vectorstock.com/i/thumb-large/81/62/default-avatar-photo-placeholder-icon-grey-vector-38508162.jpg';
  authorName: string = '';
  authorContent: string = '';
  popWindow: Boolean = false;
  authorSlug: string = 'https://images.quotable.dev/profile/400/';
  constructor(private _dataservice: DataServiceService) {}

  ngOnInit(): void {
    this._dataservice.getAuthorList().subscribe((response) => {
      this.authors = response;
      this.authors = this.authors['results'];
    });
  }
  viewAuthorDetails(description: string, name: string) {
    this.authorName = name;
    this.authorContent = description;
    this.popWindow = true;
  }
  closePopup() {
    this.popWindow = false;
  }
}
