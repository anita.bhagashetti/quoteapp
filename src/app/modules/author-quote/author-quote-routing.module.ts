import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthorQuoteComponent } from './author-quote.component';

const routes: Routes = [{ path: '', component: AuthorQuoteComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthorQuoteRoutingModule { }
