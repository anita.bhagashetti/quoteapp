import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthorQuoteRoutingModule } from './author-quote-routing.module';
import { AuthorQuoteComponent } from './author-quote.component';
import { RandomQuotesTemplateModule } from 'src/app/components/random-quotes-template/random-quotes-template.module';


@NgModule({
  declarations: [
    AuthorQuoteComponent
  ],
  imports: [
    CommonModule,
    AuthorQuoteRoutingModule,
    RandomQuotesTemplateModule
  ]
})
export class AuthorQuoteModule { }
