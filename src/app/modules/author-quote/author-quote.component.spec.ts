import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorQuoteComponent } from './author-quote.component';

describe('AuthorQuoteComponent', () => {
  let component: AuthorQuoteComponent;
  let fixture: ComponentFixture<AuthorQuoteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AuthorQuoteComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorQuoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
