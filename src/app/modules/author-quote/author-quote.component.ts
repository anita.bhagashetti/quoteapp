import { Component, OnInit } from '@angular/core';
import { DataServiceService } from 'src/app/services/data-service.service';

@Component({
  selector: 'app-author-quote',
  templateUrl: './author-quote.component.html',
  styleUrls: ['./author-quote.component.scss'],
})
export class AuthorQuoteComponent implements OnInit {
  authors: any;
  loader = true;
  authorQuotes: any = {
    author: '',
    content: '',
    tags: '',
    dateAdded: '',
  };
  popWindow: Boolean = false;
  authorSlug: string = 'https://images.quotable.dev/profile/400/';
  authorName: string = '';
  authorContent: string = '';
  flag: Boolean = false;
  filterAuthorName: string = '';
  quotes: any;
  constructor(private _dataService: DataServiceService) {}

  ngOnInit(): void {
    if (Object.keys(history.state).length === 2) {
      this.flag = true;
      this.filterAuthorName = history.state['0']['author'];
      this._dataService
        .getAuthorQuotes(history.state['0']['author'])
        .subscribe((response) => {
          this.quotes = response;
          this.quotes = this.quotes['results'];

          this.authorSlug =
            'https://images.quotable.dev/profile/400/' +
            history.state['0']['authorSlug'] +
            '.jpg';
          this.loader = false;
        });
    } else {
      this._dataService
        .getAuthorQuotes('Michael Jordan')
        .subscribe((response) => {
          this.quotes = response;
          this.quotes = this.quotes['results'];
          this.loader = false;
          this.authorSlug =
            'https://images.quotable.dev/profile/400/michael-jordan.jpg';
        });
    }
    this._dataService.getAuthorList().subscribe((response) => {
      this.authors = response;
      this.authors = this.authors['results'];
    });
  }
  getQuotes(event: any) {
    this._dataService
      .getAuthorQuotes(event.target.value)
      .subscribe((response) => {
        this.quotes = response;
        this.quotes = this.quotes['results'];

        this.authorSlug =
          'https://images.quotable.dev/profile/400/' +
          this.quotes[0]['authorSlug'] +
          '.jpg';
      });
  }
  viewFullScreen(
    authorname: string,
    authorcontent: string,
    tags: String,
    dateAdded: string
  ) {
    this.popWindow = true;
    this.authorQuotes['author'] = authorname;
    this.authorQuotes['content'] = authorcontent;
    this.authorQuotes['tags'] = tags;
    this.authorQuotes['dateAdded'] = dateAdded;
  }

  closePopup() {
    this.popWindow = false;
  }
  handleClick() {
    this.flag = false;
  }
}
