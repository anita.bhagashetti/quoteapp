import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RandomQuotesComponent } from './random-quotes.component';

const routes: Routes = [{ path: '', component: RandomQuotesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RandomQuotesRoutingModule {}
