import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RandomQuotesRoutingModule } from './random-quotes-routing.module';
import { RandomQuotesComponent } from './random-quotes.component';
import { RandomQuotesTemplateModule } from 'src/app/components/random-quotes-template/random-quotes-template.module';

@NgModule({
  declarations: [RandomQuotesComponent],
  imports: [
    CommonModule,
    RandomQuotesRoutingModule,
    RandomQuotesTemplateModule,
  ],
})
export class RandomQuotesModule {}
