import { Component, OnInit } from '@angular/core';
import { DataServiceService } from 'src/app/services/data-service.service';

@Component({
  selector: 'app-random-quotes',
  templateUrl: './random-quotes.component.html',
  styleUrls: ['./random-quotes.component.scss'],
})
export class RandomQuotesComponent implements OnInit {
  tags: string[] = [
    'sports',
    'religion',
    'motivational',
    'self',
    'love',
    'success',
    'famous-quotes',
    'friendship',
    'inspirational',
    'technology',
    'change',
    'wisdom',
    'philosophy',
    'humorous',
    'life',
    'happiness',
  ];
  quotes: any = {
    author: '',
    content: '',
    tags: '',
    dateAdded: '',
  };

  randomQuotes: any;
  popWindow: Boolean = false;
  constructor(private _dataService: DataServiceService) {}

  ngOnInit(): void {
    this._dataService.getRandomQuotes('').subscribe((response) => {
      this.randomQuotes = response;
      this.randomQuotes = this.randomQuotes.results;
    });
  }

  getTagValue($event: any) {
    this._dataService
      .getRandomQuotes($event.target.value)
      .subscribe((response) => {
        this.randomQuotes = response;
        this.randomQuotes = this.randomQuotes.results;
      });
  }
  viewFullQuote(
    authorname: string,
    authorcontent: string,
    tags: String,
    dateAdded: string
  ) {
    this.popWindow = true;
    this.quotes['author'] = authorname;
    this.quotes['content'] = authorcontent;
    this.quotes['tags'] = tags;
    this.quotes['dateAdded'] = dateAdded;
  }
  closePopup() {
    this.popWindow = false;
    // this.quotes['author'] = '';
    // this.quotes['content'] = '';
  }
}
