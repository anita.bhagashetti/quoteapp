import { Component, OnInit } from '@angular/core';
import { DataServiceService } from 'src/app/services/data-service.service';

@Component({
  selector: 'app-full-randomquotes',
  templateUrl: './full-randomquotes.component.html',
  styleUrls: ['./full-randomquotes.component.scss'],
})
export class FullRandomquotesComponent implements OnInit {
  data: any = {
    tags: 'Wisdom',
    content:
      'An appeaser is one who feeds a crocodile, hoping it will eat him last.',
    author: 'Winston Churchill',
    dateAdded: '2022-03-12',
  };
  constructor(private _dataService: DataServiceService) {}

  ngOnInit(): void {
    setInterval(() => {
      this._dataService.getFullRandomQuotes().subscribe((response) => {
        this.data = response;
      });
    }, 5000);
  }
}
