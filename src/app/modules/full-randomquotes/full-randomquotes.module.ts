import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FullRandomquotesRoutingModule } from './full-randomquotes-routing.module';
import { FullRandomquotesComponent } from './full-randomquotes.component';
// import { RandomQuotesModule } from '../random-quotes/random-quotes.module';
// import { RandomQuotesModule } from 'src/app/components/random-quotes-template/random-quotes-template.module';
import { RandomQuotesTemplateModule } from 'src/app/components/random-quotes-template/random-quotes-template.module';

@NgModule({
  declarations: [FullRandomquotesComponent],
  imports: [
    CommonModule,
    FullRandomquotesRoutingModule,
    RandomQuotesTemplateModule,
  ],
})
export class FullRandomquotesModule {}
