import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FullRandomquotesComponent } from './full-randomquotes.component';

const routes: Routes = [{ path: '', component: FullRandomquotesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FullRandomquotesRoutingModule {}
