import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FullRandomquotesComponent } from './full-randomquotes.component';

describe('FullRandomquotesComponent', () => {
  let component: FullRandomquotesComponent;
  let fixture: ComponentFixture<FullRandomquotesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FullRandomquotesComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FullRandomquotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
