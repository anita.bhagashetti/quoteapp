import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StaticPageParent2RoutingModule } from './static-page-parent2-routing.module';
import { StaticPageParent2Component } from './static-page-parent2.component';
import { StaticPagePresentationModule } from 'src/app/components/static-page-presentation/static-page-presentation.module';

@NgModule({
  declarations: [StaticPageParent2Component],
  imports: [
    CommonModule,
    StaticPageParent2RoutingModule,
    StaticPagePresentationModule,
  ],
})
export class StaticPageParent2Module {}
