import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-static-page-parent2',
  templateUrl: './static-page-parent2.component.html',
  styleUrls: ['./static-page-parent2.component.scss'],
})
export class StaticPageParent2Component implements OnInit {
  staticData: any = {
    head: 'Train smarter, get stronger',
    head1: 'Simple fitness experince for everyone',
    button: 'Download App',
    button1: 'Book a Class',
    img: 'assets/3d-morph-man-exercising-with-gym-weights_1048-13685-removebg-preview.png',
    about:
      'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Qui aut optio,ipsum dolor sit amet consectetur adipisicing elit. Qui aut',
  };

  constructor() {}

  ngOnInit(): void {}
}
