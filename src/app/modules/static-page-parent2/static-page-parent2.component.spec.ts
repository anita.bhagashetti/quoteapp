import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticPageParent2Component } from './static-page-parent2.component';

describe('StaticPageParent2Component', () => {
  let component: StaticPageParent2Component;
  let fixture: ComponentFixture<StaticPageParent2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StaticPageParent2Component],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticPageParent2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
