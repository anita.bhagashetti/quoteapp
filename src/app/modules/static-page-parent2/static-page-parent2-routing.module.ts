import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StaticPageParent2Component } from './static-page-parent2.component';

const routes: Routes = [{ path: '', component: StaticPageParent2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StaticPageParent2RoutingModule {}
