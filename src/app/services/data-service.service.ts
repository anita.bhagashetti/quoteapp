import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class DataServiceService {
  private random_quotes_url = 'https://quotable.io/quotes?tags=';
  private authour_list_url =
    'https://quotable.io/authors?limit=150';
  private authorQuotes_url = 'https://quotable.io/quotes?author=';
  private full_random_quotes_url = 'https://api.quotable.io/random';

  constructor(private httpClient: HttpClient) {}
  getAuthorList() {
    return this.httpClient.get(this.authour_list_url);
  }
  getAuthorQuotes(authorName: string) {
    this.authorQuotes_url = 'https://quotable.io/quotes?author=' + authorName;
    return this.httpClient.get(this.authorQuotes_url);
  }
  getRandomQuotes(tag: string) {
    this.random_quotes_url = 'https://quotable.io/quotes?tags=' + tag;
    return this.httpClient.get(this.random_quotes_url);
  }
  getFullRandomQuotes() {
    return this.httpClient.get(this.full_random_quotes_url);
  }
  
}
