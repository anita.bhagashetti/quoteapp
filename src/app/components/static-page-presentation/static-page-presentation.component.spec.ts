import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticPagePresentationComponent } from './static-page-presentation.component';

describe('StaticPagePresentationComponent', () => {
  let component: StaticPagePresentationComponent;
  let fixture: ComponentFixture<StaticPagePresentationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StaticPagePresentationComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticPagePresentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
