import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-static-page-presentation',
  templateUrl: './static-page-presentation.component.html',
  styleUrls: ['./static-page-presentation.component.scss'],
})
export class StaticPagePresentationComponent implements OnInit {
  constructor(private router: Router) {}

  @Input()staticData: any;
  ngOnInit(): void {}
  isStaticpage2() {
    return this.router.url === '/staticpage2';
  }
  isStaticpage1() {
    return this.router.url === '/staticpage1';
  }
}
