import { NgModule } from '@angular/core';
import { StaticPagePresentationComponent } from './static-page-presentation.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [StaticPagePresentationComponent],
  imports: [CommonModule],
  exports: [StaticPagePresentationComponent],
  providers: [],
  bootstrap: [],
})
export class StaticPagePresentationModule {}
