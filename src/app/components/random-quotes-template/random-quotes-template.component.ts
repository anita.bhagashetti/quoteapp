import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-random-quotes-template',
  templateUrl: './random-quotes-template.component.html',
  styleUrls: ['./random-quotes-template.component.scss'],
})
export class RandomQuotesTemplateComponent implements OnInit {
  @Input() public data: any;
  @Input('popWindow') public popWindow: any;
  @Output() public popUpCloseEvent = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}
  ngOnChanges(): void {
    console.log(this.popWindow);
  }
  popClose() {
    this.popUpCloseEvent.emit();
  }
}
