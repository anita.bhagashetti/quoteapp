import { NgModule } from '@angular/core';
import { RandomQuotesTemplateComponent } from './random-quotes-template.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [RandomQuotesTemplateComponent],
  imports: [CommonModule],
  exports: [RandomQuotesTemplateComponent],
})
export class RandomQuotesTemplateModule {}
