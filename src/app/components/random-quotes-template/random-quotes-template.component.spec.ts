import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RandomQuotesTemplateComponent } from './random-quotes-template.component';

describe('RandomQuotesTemplateComponent', () => {
  let component: RandomQuotesTemplateComponent;
  let fixture: ComponentFixture<RandomQuotesTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RandomQuotesTemplateComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RandomQuotesTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
